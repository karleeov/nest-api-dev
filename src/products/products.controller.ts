import { Controller, Post, Body } from '@nestjs/common';
import { ProductsService } from './products.services';

@Controller('products')
export class ProductController {
  constructor(private readonly productSerivce: ProductsService) {}
  @Post()
  addProduct(
    @Body('title') prodTitle: string,
    @Body('description') prodDesc: string,
    @Body('price') prodPrice: number,
  ): any {
    const generatedId = this.productSerivce.insertProduct(
      prodTitle,
      prodDesc,
      prodPrice,
    );
    return { id: generatedId };
  }
}
